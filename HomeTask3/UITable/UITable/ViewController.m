//
//  ViewController.m
//  UITable
//
//  Created by Valiantsin Vasiliavitski on 4/2/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"

typedef NS_ENUM(NSUInteger, TableSection) {
    TableSectionActions = 0,
    TableSectionLogs = 1,
};

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, strong) NSArray *fields;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Table Task";
    self.fields = @[@"Back", @"Push", @"Present modal", @"Log section number", @"Log row number", @"Log table frame", @"Log cell frame"];
//    [self.table registerNib:[UINib nibWithNibName:@"CustomCell" bundle:nil] forCellReuseIdentifier:@"CustomCell"];
    [self.table registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CustomCell"];
    
    // Do any additional setup after loading the view, typically from a nib.
//    UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
//    tableView.delegate = self;
//    tableView.dataSource = self;
    
//    [self.view addSubview:tableView];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == TableSectionActions) {
        return 3;
    } else {
        return 4;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch(indexPath.section) {
        case TableSectionActions:
            [self tableSectionLogs:indexPath];
            break;
            
        case TableSectionLogs:
            [self tableSectionsActions:indexPath];
            break;

    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)tableSectionsActions:(NSIndexPath *)indexPath {
    switch(indexPath.row) {
        case 0:
            NSLog(@"Section number: %ld", indexPath.section);
            break;
        case 1:
            NSLog(@"Row number: %ld", indexPath.row);
            break;
        case 2:
            NSLog(@"Table frame: origin: x=%f, y=%f, size: height=%f, width=%f", self.table.frame.origin.x, self.table.frame.origin.x, self.table.frame.size.height, self.table.frame.size.width);
            break;
        case 3: {
            UITableViewCell *cell = [self.table cellForRowAtIndexPath:indexPath];
            NSLog(@"Table frame: origin: x=%f, y=%f, size: height=%f, width=%f", cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.height, cell.frame.size.width);
            
            break;
        }
    }
}

-(void)tableSectionLogs:(NSIndexPath *)indexPath {
    switch(indexPath.row) {
        case 0:
            //                    [self.navigationController parentViewControllers];
            [self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            
            break;
        case 1:{
            UIViewController *view  =[self.storyboard instantiateViewControllerWithIdentifier:@"viewController"];
            [self.navigationController pushViewController:view animated:YES];
            break;
        }
        case 2:{
            UINavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"nav"];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
            break;
        }
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return @"Navigation actions";
    }
    return @"Log actions";
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CustomCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    if ([indexPath section] == 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = [self.fields objectAtIndex:indexPath.row];
    } else {
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
        cell.textLabel.text = [self.fields objectAtIndex:indexPath.row + 3];
    }
    
    return cell;
}


@end
