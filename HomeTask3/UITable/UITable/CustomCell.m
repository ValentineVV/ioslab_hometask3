//
//  CustomCell.m
//  UITable
//
//  Created by Valiantsin Vasiliavitski on 4/3/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
