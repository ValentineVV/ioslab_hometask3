//
//  CustomCell.h
//  UITable
//
//  Created by Valiantsin Vasiliavitski on 4/3/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cellView;

@end
